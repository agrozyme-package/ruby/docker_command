require('docker_core')

module DockerCore
  module Process
    SUDO = true
  end

  module Swarm
    NAMESPACE = 'agrozyme'

    def self.detect_services
      return { docker: Shell.is_active_unit('docker'), k0s: Shell.is_active_unit('k0scontroller'), k3s: Shell.is_active_unit('k3s') }
    end

    def self.load_commands
      path = File.join(__dir__, 'command', '*.rb')
      return Dir.glob(path, &method(:require))
    end
  end

  class Runner < Thor
    desc('swarm_setup', 'setup swarm')
    def swarm_setup
      Error.no_method(__method__)
    end

    desc('swarm_leave', 'leave swarm')
    def swarm_leave
      Error.no_method(__method__)
    end

    desc('stack_deploy', 'deploy stack')
    def stack_deploy
      Error.no_method(__method__)
    end

    desc('stack_remove', 'remove stack')
    def stack_remove
      Error.no_method(__method__)
    end
  end
end
