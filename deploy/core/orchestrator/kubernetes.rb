require_relative('../swarm')

module DockerCore
  module Orchestrator
    module Kubernetes
      def self.prefix_command
        return ''
      end

      def self.certificate_manager
        repository = 'jetstack/cert-manager'
        version = Shell.github_latest_version(repository)
        return "https://github.com/#{repository}/releases/download/#{version}/cert-manager.yaml"
      end

      def self.swarm_setup
        Process.run(self.prefix_command, 'kubectl apply', { filename: self.certificate_manager }, '')
      end

      def self.swarm_leave
        Process.run(self.prefix_command, 'kubectl delete', { filename: self.certificate_manager }, '')
      end

      def self.stack_deploy
        Swarm.prepare_folders
        Process.run(self.prefix_command, 'kubectl apply', { kustomize: File.join(self.deploy_path, 'stack') }, '')
      end

      def self.stack_remove
        Process.run(self.prefix_command, 'kubectl delete', { kustomize: File.join(self.deploy_path, 'stack') }, '')
      end

      def self.hash_digest(text)
        return Digest::MD5::hexdigest("#{text}")
      end

      # @param [String] image
      # @param [Hash] bind
      def self.override_manifest(name, image, *arguments, bind: {})
        volumes = []
        mounts = []

        bind.each do |volume, mount|
          bind_name = self.hash_digest(volume)
          volumes << { name: bind_name, hostPath: { path: "#{volume}" } }
          mounts << { name: bind_name, mountPath: "#{mount}" }
        end

        user = ::Process.uid
        group = ::Process.gid
        context = { runAsUser: user, runAsGroup: group }

        parameters = Paser.arguments(*arguments)
        container = { name: "#{name}", image: "#{image}", imagePullPolicy: 'Always', volumeMounts: mounts, args: parameters }
        manifest = { spec: { securityContext: context, volumes: volumes, containers: [container] } }
        return JSON.generate(manifest)
      end

      # @param [String] image
      # @param [Array] arguments
      # @param [Hash] environment
      # @param [Hash] volumes
      def self.prepare_flags(image, *arguments, environment: {}, bind: {})
        name = self.hash_digest(Time.now)
        flags = [name, { image: "#{image}", restart: 'Never', rm: true, stdin: true, tty: true }]

        environment.each do |key, value|
          flags << { env: "#{key}=#{value}" }
        end

        flags << { overrides: self.override_manifest(name, image, *arguments, bind: bind) }
        return flags
      end

      # @param [String] image
      # @param [Array] arguments
      # @param [Hash] environment
      # @param [Hash] bind
      def self.prepare_command(image, *arguments, environment: {}, bind: {})
        flags = self.prepare_flags(image, *arguments, environment: environment, bind: bind)
        return [self.prefix_command, 'kubectl run', *flags, '']
      end

    end

  end

  module Swarm
    # @param [String] image
    # @param [Array] arguments
    # @param [Hash] environment
    # @param [Hash] bind
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] strip
    # @param [Boolean] echo
    def self.capture_command(image, *arguments, environment: {}, bind: {}, throw: false, wait: 0, strip: true, echo: false)
      command = Orchestrator::Kubernetes.prepare_command(image, *arguments, environment: environment, bind: bind)
      return Process.capture(*command, throw: throw, wait: wait, strip: strip, echo: echo)
    end

    # @param [String] image
    # @param [Array] arguments
    # @param [Hash] environment
    # @param [Hash] bind
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] echo
    def self.run_command(image, *arguments, environment: {}, bind: {}, throw: true, wait: 0, echo: true)
      command = Orchestrator::Kubernetes.prepare_command(image, *arguments, environment: environment, bind: bind)
      return Process.run(*command, throw: throw, wait: wait, echo: echo)
    end

  end

  class Runner < Thor
    desc('kubectl [options]', 'execute kubectl command')
    def kubectl(*arguments)
      Process.run(Orchestrator::Kubernetes.prefix_command, 'kubectl', *arguments)
    end

    desc('swarm_setup', 'setup kubernetes swarm')
    def swarm_setup
      Orchestrator::Kubernetes.swarm_setup
    end

    desc('swarm_leave', 'leave kubernetes swarm')
    def swarm_leave
      Orchestrator::Kubernetes.swarm_leave
    end

    desc('stack_deploy', 'deploy kubernetes stack')
    def stack_deploy
      Orchestrator::Kubernetes.stack_deploy
    end

    desc('stack_remove', 'remove kubernetes stack')
    def stack_remove
      Orchestrator::Kubernetes.stack_remove
    end

  end

end
