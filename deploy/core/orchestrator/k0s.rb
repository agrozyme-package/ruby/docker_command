require_relative('./kubernetes')

module DockerCore
  module Orchestrator
    module Kubernetes
      def self.prefix_command
        return 'k0s'
      end
    end
  end
end
