require_relative('../swarm')

module DockerCore
  module Orchestrator
    module Docker
      def self.docker_compose
        return File.join(Swarm.deploy_path, 'stack', 'docker-compose.yml')
      end

      def self.advertise_address
        list = Socket.ip_address_list.filter do

          # @type [Addrinfo] address
        |address|
          if address.ipv4?
            next false == address.ipv4_loopback? && false == address.ipv4_multicast? && false == address.ipv4_private?
          end

          if address.ipv6?
            next false == address.ipv6_loopback? && false == address.ipv6_multicast? && false == address.ipv6_linklocal?
          end

          next false
        end.sort_by do

          # @type [Addrinfo] address
        |address|
          next address.ipv6? ? -1 : 1
        end.map do

          # @type [Addrinfo] address
        |address|
          next address.ip_address
        end

        return 0 < list.size ? list[0] : ''
      end

      def self.swarm_state
        return Process.capture('docker info', { format: '{{.Swarm.LocalNodeState}}' }, '')
      end

      def self.swarm_initialize
        address = self.advertise_address
        options = address.empty? ? {} : { 'advertise-addr': address }
        Process.run('docker swarm init', options, wait: 1)
      end

      def self.swarm_leave
        self.stack_remove

        if 'inactive' != self.swarm_state
          Process.run('docker swarm leave --force', wait: 1)
        end

        return Process.run('docker system prune --all --force', wait: 1)
      end

      def self.network_name
        return ENV.fetch('DOCKER_NETWORK', 'network')
      end

      def self.check_network
        network = self.network_name
        name = Process.capture('docker network ls', { filter: "name=#{network}", format: '{{.Name}}' }, '')
        return network == name ? network : ''
      end

      def self.network_create
        return Process.run('docker network create', { attachable: true, driver: 'overlay' }, self.network_name, wait: 1)
      end

      def self.network_remove
        return Process.run('echo y | docker network rm', self.network_name, wait: 1, throw: false)
      end

      def self.network_attachable
        network = self.check_network
        return '' != network && 'true' == Process.capture('docker network inspect', { format: '{{.Attachable}}' }, network)
      end

      def self.container_stop
        containers = Process.capture('docker ps --all --quiet').lines(chomp: true)

        if 0 < containers.size
          Process.run('docker container stop', containers, wait: 1)
        end
      end

      def self.container_prune
        return Process.run('docker container prune --force', wait: 1)
      end

      def self.stack_remove
        stacks = Process.capture('docker stack ls', { format: '{{.Name}}' }, '').lines(chomp: true)

        if 0 < stacks.size
          Process.run('docker stack rm stop', stacks, wait: 1)
          self.container_stop
        end

        self.container_prune
      end

      # @return [Array<String>]
      def self.compose_services
        # @type [Hash] yaml
        yaml = YAML.load_file(self.docker_compose)

        if false == yaml
          return []
        end

        # @type [Hash] services
        services = yaml.fetch('services', {})

        if services.respond_to?(:keys)
          return services.method(:keys).call
        end

        return []
      end

      def self.stack_deploy
        setup = File.join(Swarm.deploy_path, 'stack', 'setup.sh')
        compose = self.docker_compose

        if false == File.file?(compose)
          return
        end

        if File.file?(setup)
          Process.run('bash', setup, wait: 1)
        end

        Swarm.prepare_folders(*self.compose_services)
        Process.run('docker stack deploy', { 'compose-file': compose }, 'stack', wait: 1)
      end

      def self.swarm_setup
        if 'error' == self.swarm_state
          self.swarm_leave
        end

        if 'inactive' == self.swarm_state
          self.swarm_initialize
        end

        if false == self.network_attachable
          self.network_remove
          self.network_create
        end
      end

      # @param [Hash] environment
      # @param [Hash] bind
      def self.prepare_flags(environment: {}, bind: {})
        user = ::Process.uid
        group = ::Process.gid
        network = self.check_network
        flags = [{ rm: true, interactive: true, tty: true, user: "#{user}:#{group}" }]

        if '' != network
          flags << { network: network }
        end

        environment.each do |key, value|
          flags << { env: "#{key}=#{value}" }
        end

        bind.each do |host, mount|
          flags << { volume: "#{host}:#{mount}" }
        end

        return flags
      end

      # @param [String] image
      # @param [Array] arguments
      # @param [Hash] environment
      # @param [Hash] bind
      def self.prepare_command(image, *arguments, environment: {}, bind: {})
        flags = self.prepare_flags(environment: environment, bind: bind)
        return ['docker run', *flags, "#{image}", *arguments]
      end
    end
  end

  module Swarm
    # @param [String] image
    # @param [Array] arguments
    # @param [Hash] environment
    # @param [Hash] bind
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] strip
    # @param [Boolean] echo
    def self.capture_command(image, *arguments, environment: {}, bind: {}, throw: false, wait: 0, strip: true, echo: false)
      command = Orchestrator::Docker.prepare_command(image, *arguments, environment: environment, bind: bind)
      return Process.capture(*command, throw: throw, wait: wait, strip: strip, echo: echo)
    end

    # @param [String] image
    # @param [Array] arguments
    # @param [Hash] environment
    # @param [Hash] bind
    # @param [Boolean] throw
    # @param [Numeric] wait
    # @param [Boolean] echo
    def self.run_command(image, *arguments, environment: {}, bind: {}, throw: true, wait: 0, echo: true)
      command = Orchestrator::Docker.prepare_command(image, *arguments, environment: environment, bind: bind)
      return Process.run(*command, throw: throw, wait: wait, echo: echo)
    end
  end

  class Runner < Thor
    desc('swarm_setup', 'setup docker swarm')
    def swarm_setup
      Orchestrator::Docker.swarm_setup
    end

    desc('swarm_leave', 'leave docker swarm')
    def swarm_leave
      Orchestrator::Docker.swarm_leave
    end

    desc('stack_deploy', 'deploy docker stack')
    def stack_deploy
      Orchestrator::Docker.stack_deploy
    end

    desc('stack_remove', 'remove docker stack')
    def stack_remove
      Orchestrator::Docker.stack_remove
    end
  end
end
