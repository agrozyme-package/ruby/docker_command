require_relative('./base')
require('pathname')

module DockerCore
  module Command
    class Drupal
      include(Base::Command)

      # @return [String]
      attr_reader :root

      # @return [String]
      attr_reader :relative

      # @return [Boolean]
      attr_reader :connected

      def self.from_image
        return Swarm.from_image('drupal')
      end

      def self.work_folder
        return '/var/www/html'
      end

      def self.deploy_module
        return 'site_deploy'
      end

      def initialize
        @root = self.status_field('root')
        @relative = @root.empty? ? '' : Pathname.new(@root).relative_path_from(self.class.work_folder).to_s
        @connected = 'Connected' == self.status_field('db-status')
      end

      # @param [String] name
      def status_field(name)
        self.class.capture_command('drush core:status', { fields: name, format: 'list' }, '')
      end

      def check_status
        color = Color::YELLOW
        has_root = false == @root.empty?

        unless has_root
          Color.echo('Not in drupal root directory', color)
        end

        unless @connected
          Color.echo('Not connected database or database setting error', color)
        end

        has_root && @connected
      end

      # @param [String] name
      def check_export(name)
        self.check_status && Swarm.check_name(name, echo: true)
      end

      def set_sites_default_writable
        folder = File.join(Dir.pwd, @relative, 'sites', 'default')

        if Dir.exist?(folder)
          Shell.change_mode('a+w', folder)
        end

        nil
      end

      def clear_cache
        self.set_sites_default_writable
        folder = self.status_field('files')

        if folder.empty?
          Color.echo('No find cache folder', Color::YELLOW)
          return
        end

        items = %w[css js languages translations].map do |item|
          next File.join(Dir.pwd, 'web', folder, item)
        end

        Shell.clear_folders(*items)
        self.class.run_command('drush cache:rebuild')
      end

      # @param [Array] arguments
      def enable_modules(*arguments)
        if 0 < arguments.size
          self.class.run_command('drush pm:enable', { yes: true }, arguments.uniq.join(','))
        end
      end

      # @param [Array] arguments
      def uninstall_modules(*arguments)
        if 0 < arguments.size
          self.class.run_command('drush pm:uninstall', { yes: true }, arguments.uniq.join(','))
        end
      end

      # @param [String] name
      def config_paths(name)
        folder = File.join('export', 'config', name)
        data = Swarm.pair_paths(self.class.work_folder, folder)
        data[:folder] = data[:outside]
        data[:file] = data[:folder] + '.tar.gz'
        data
      end

      # @param [String] name
      # @param [Boolean] clear
      def config_export(name, clear: false)
        data = self.config_paths(name)
        Shell.clear_folders(data[:folder])

        if clear
          self.clear_cache
        end

        self.class.run_command('drush config:export', { destination: data[:inside] }, '')
        Shell.deflate_file(data[:file], data[:folder])

        Shell.remove_paths(data[:folder])
        Color.echo("#{name} config export finish", Color::CYAN)
      end

      # @param [String] name
      # @param [Boolean] clear
      def config_import(name, clear: false)
        data = self.config_paths(name)

        if false == File.file?(data[:file])
          Color.echo("#{name} config exported file is not exists", Color::YELLOW)
          return
        end

        if clear
          self.clear_cache
        end

        Shell.inflate_file(data[:file], data[:folder])
        self.class.run_command('drush config:import', { yes: true, source: data[:inside] }, '')

        Shell.remove_paths(data[:folder])
        Color.echo("#{name} config import finish", Color::CYAN)
      end

      # @param [String] name
      def database_paths(name)
        folder = File.join('export', 'database', name)
        data = Swarm.pair_paths(self.class.work_folder, File.join(folder, 'database.sql'))
        data[:folder] = File.dirname(data[:outside])
        data[:file] = data[:folder] + '.tar.gz'
        data
      end

      # @param [String] name
      # @param [Boolean] clear
      def database_export(name, clear: false)
        data = self.database_paths(name)
        Shell.clear_folders(data[:folder])

        if clear
          self.clear_cache
        end

        self.class.run_command('drush sql:dump > ', data[:outside])
        Shell.deflate_file(data[:file], data[:folder])

        Shell.remove_paths(data[:folder])
        Color.echo("#{name} database export finish", Color::CYAN)
      end

      # @param [String] name
      # @param [Boolean] clear
      def database_import(name, clear: false)
        data = self.database_paths(name)

        if false == File.file?(data[:file])
          Color.echo("#{name} database exported file is not exists", Color::YELLOW)
          return
        end

        Shell.clear_folders(data[:folder])
        Shell.inflate_file(data[:file], data[:folder])
        self.class.run_command('drush sql:drop', { yes: true }, '')
        self.class.run_command('drush sql:query', { file: data[:inside] }, '')

        if clear
          self.clear_cache
        end

        Shell.remove_paths(data[:folder])
        Color.echo("#{name} database import finish", Color::CYAN)
      end

      # @param [Hash] requires
      def require_packages(requires)
        hash = {}
        text = self.class.capture_command('composer show', { format: 'json' }, '')
        json = JSON.parse(text)

        [json['installed']].flatten.each do # @type [Hash<String, String>] item
        |item|
          key = item['name']
          value = item['version'].split(' ').fetch(0, '')
          hash[key] = value
        end

        #noinspection RubyUnusedLocalVariable
        requires.filter do |key, value|
          items = "#{key}".split(':', 2)
          name = items.fetch(0, '')
          version = items.fetch(1, '')

          if false == hash.has_key?(name)
            next true
          end

          if version.empty?
            next false
          end

          next hash[name] != version
        end

      end

      def setup_default_content_deploy
        requires = { #
          # 'drupal/better_normalizers': ['better_normalizers'],
          'drupal/default_content': ['default_content'], #
          # 'drupal/default_content_deploy:1.x-dev@dev': ['default_content_deploy'], #
          'drupal/default_content_deploy:dev-1.x': ['default_content_deploy'], #
          # 'drupal/default_content_deploy': ['default_content_deploy'], #
          'drupal/default_content_extra': ['default_content_extra']
        }

        requires = self.require_packages(requires)
        packages = []
        modules = []

        requires.each do |package, items|
          packages << package

          items.each do |item|
            modules << item
          end
        end

        if 0 < packages.size
          self.class.run_command('composer', { 'no-interaction': true }, 'require', packages.uniq, environment: { COMPOSER_DISCARD_CHANGES: 'true' })
        end

        self.enable_modules(*modules)
      end

      # @param [String] name
      def content_paths(name)
        folder = File.join('export', 'content', name)
        data = Swarm.pair_paths(self.class.work_folder, folder)
        data[:folder] = folder
        data[:file] = data[:folder] + '.tar.gz'
        data
      end

      # @param [String] name
      # @param [Boolean] clear
      def content_export(name, clear: false)
        data = self.content_paths(name)
        Shell.clear_folders(data[:folder])
        self.setup_default_content_deploy

        if clear
          self.clear_cache
        end

        self.class.run_command('drush default-content-deploy:export-site', { folder: data[:inside] }, '')
        Shell.deflate_file(data[:file], data[:folder])

        Shell.remove_paths(data[:folder])
        Color.echo("#{name} content export finish", Color::CYAN)
      end

      # @param [String] name
      # @param [Boolean] clear
      def content_import(name, clear: false)
        data = self.content_paths(name)
        data[:folder] = File.join(Dir.pwd, 'content')

        if false == File.file?(data[:file])
          Color.echo("#{name} content exported file is not exists", Color::YELLOW)
          return
        end

        Shell.clear_folders(data[:folder])
        self.clear_cache
        Shell.inflate_file(data[:file], data[:folder])
        self.setup_default_content_deploy

        if clear
          self.clear_cache
        end

        self.class.run_command('drush default-content-deploy:import', { yes: true, 'force-override': true, 'preserve-password': true }, '')
        # self.class.run_drush('default-content-deploy:import', { yes: true, 'force-override': true, 'preserve-password': true }, { folder: data[:inside] }, '')
        Shell.remove_paths(data[:folder])
        Color.echo("#{name} content export finish", Color::CYAN)
      end

      def deploy_paths(name)
        current = Dir.pwd
        hash = { current: current, export: File.join(current, 'export', 'deploy'), module: File.join(current, @relative, 'modules', 'custom', self.class.deploy_module) }
        hash[:file] = File.join(hash[:export], "#{name}.info.yml")
        hash
      end

      def deploy_module_content
        name = self.class.deploy_module
        info = []
        info << "name: #{name}"
        info << "description: #{name}"
        info << 'type: module'
        info << 'core: 8.x'
        info << 'core_version_requirement: ^8 || ^9'
        # info << 'hidden: true'
        info << 'dependencies:'

        self.class.capture_command('drush pm:list', { type: 'module', status: 'enabled', field: 'name' }, '').lines(chomp: true).filter do |item|
          next item != name
        end.each do |item|
          info << "  - #{item}"
        end

        info.join("\n")
      end

      # @param [String] name
      def update_deploy_module(name)
        data = self.deploy_paths(name)
        composer_json = 'composer.json'
        current_composer_json = File.join(data[:current], composer_json)
        export_composer_json = File.join(data[:export], "#{name}.#{composer_json}")

        Shell.make_folders(data[:export])
        File.write(data[:file], self.deploy_module_content)

        if File.exist?(current_composer_json)
          FileUtils.cp(current_composer_json, export_composer_json)
        end
      end

      def update_database
        if 'Enabled' == self.class.capture_command('drush pm:list', { type: 'module', field: 'status', filter: 'name=locale' }, '')
          self.class.run_command('drush locale:check')
          self.class.run_command('drush locale:update')
        end

        self.class.run_command('drush updatedb', { yes: true }, '')
        self.clear_cache
      end

      # @param [String] name
      def deploy_export(name)
        self.clear_cache
        self.update_deploy_module(name)
        self.database_export(name)
        self.config_export(name)
        self.content_export(name)
        Color.echo("#{name} deploy export finish", Color::CYAN)
      end

      # @param [String] name
      def deploy_import(name)
        deploy = self.class.deploy_module
        data = self.deploy_paths(name)

        if false == File.file?(data[:file])
          Color.echo("#{name} deploy exported file is not exists", Color::YELLOW)
          return
        end

        Shell.clear_folders(data[:module])
        FileUtils.cp(data[:file], File.join(data[:module], "#{deploy}.info.yml"))

        self.clear_cache
        self.uninstall_modules(deploy)
        self.config_import(name)
        self.enable_modules(deploy)
        self.content_import(name)
        self.uninstall_modules(deploy)
        self.update_database

        Shell.remove_paths(data[:module])
        Color.echo("#{name} deploy import finish", Color::CYAN)
      end
    end

  end

  class Runner < Thor
    desc('drush [options]', 'execute drush command')
    def drush(*arguments)
      Command::Drupal.run_command(__method__, *arguments)
    end

    desc('drupal_clear_cache', 'clear drupal cache files')
    def drupal_clear_cache
      drupal = Command::Drupal.new

      if drupal.check_status
        drupal.clear_cache
      end
    end

    desc('drupal_config_export NAME', 'drupal config export')
    def drupal_config_export(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.config_export(name, clear: true)
      end
    end

    desc('drupal_config_import NAME', 'import drupal config')
    def drupal_config_import(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.config_import(name, clear: true)
      end
    end

    desc('drupal_database_export NAME', 'export drupal database')
    def drupal_database_export(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.database_export(name, clear: true)
      end
    end

    desc('drupal_database_import NAME', 'import drupal database')
    def drupal_database_import(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.database_import(name, clear: true)
      end
    end

    desc('drupal_content_export NAME', 'export drupal content')
    def drupal_content_export(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.content_export(name, clear: true)
      end
    end

    desc('drupal_content_import NAME', 'import drupal content')
    def drupal_content_import(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.content_import(name, clear: true)
      end
    end

    desc('drupal_deploy_export NAME', 'export drupal deploy')
    def drupal_deploy_export(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.deploy_export(name)
      end
    end

    desc('drupal_deploy_import NAME', 'import drupal deploy')
    def drupal_deploy_import(name)
      drupal = Command::Drupal.new

      if drupal.check_export(name)
        drupal.deploy_import(name)
      end
    end
  end

end
