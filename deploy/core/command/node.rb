require_relative('./base')

module DockerCore
  module Command
    module Node
      include(Base::Command)

      def self.from_image
        return Swarm.from_image('node')
      end

      def self.work_folder
        return '/var/www/html'
      end
    end
  end

  class Runner < Thor
    desc('npm [options]', 'execute npm command')
    def npm(*arguments)
      Command::Node.run_command(__method__, *arguments, throw: false)
    end
  end

end
