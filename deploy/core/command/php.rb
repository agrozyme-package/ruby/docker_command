require_relative('./base')

module DockerCore
  module Command
    module Php
      include(Base::Command)

      def self.from_image
        return Swarm.from_image('php')
      end

      def self.work_folder
        return '/var/www/html'
      end
    end
  end

  class Runner < Thor
    desc('php [options]', 'execute php command')
    def php(*arguments)
      Command::Php.run_command(__method__, *arguments)
    end

    desc('composer [options]', 'execute composer command')
    def composer(*arguments)
      Command::Php.run_command(__method__, *arguments)
    end
  end
end
