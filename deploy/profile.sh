#! /bin/bash

function main() {
  local swarm="${1:-}"
  local path=$(dirname "${BASH_SOURCE[0]}")
  local runner="${path}/runner.rb"
  sudo chmod +x "${path}"/*

#  if [[ "" != "${swarm}" ]]; then
#    ruby "${runner}" update_swarm "${swarm}"
#  fi

  ruby "${runner}" update_alias
  source "${HOME}"/.bash_aliases
  ruby "${runner}" swarm_status
}

main "$@"
