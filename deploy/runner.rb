#! /usr/bin/env ruby

require_relative('./core/swarm')
DockerCore::Swarm.load_commands

module DockerCore
  class Runner < Thor
    desc('swarm_run', 'run command')
    def swarm_run
      Process.run('ruby', Swarm.runner_path, sudo: false)
    end

    desc('swarm_status', 'show swarm service status')
    def swarm_status
      Swarm.swarm_status
    end

    desc('update_swarm [orchestrator]', 'update default orchestrator')
    def update_swarm(orchestrator = '')
      Swarm.update_swarm(orchestrator)
    end

    desc('update_core', 'update gem docker_core')
    def update_core
      Process.run('gem update docker_core')
      Process.run('gem clean')
    end

    desc('update_alias', 'update alias')
    def update_alias
      runner = Swarm.runner_path
      method = __method__.to_s
      items = ['unalias -a']

      #noinspection RubyUnusedLocalVariable
      self.class.all_commands.filter do |key, value|
        next false == ['help', method].include?("#{key}")
      end.each do |key, value|
        items << "alias #{key}='#{runner} #{key}' "
      end

      items << "alias ls='ls --color' \n"
      items << "alias #{method}='source #{Swarm.profile_path}' \n"
      File.write(File.expand_path('~/.bash_aliases'), items.join("\n"))
      Color.echo("Update #{items.size} aliases", Color::GREEN)
    end
  end
end

DockerCore::Runner.start(ARGV)
